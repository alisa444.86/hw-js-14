const navList = $('.tabs');
const navItems = $('.tabs-title'); //document.querySelectorAll
const contentListItems = $('.tabs-content li'); //contentList.querySelectorAll
let prevMenuItem = null;
let prevText = null;

$.each(navItems, (i, item) => {
    $(item).attr('dataNumber', `${i}`);
    $(contentListItems[i]).attr('dataNumber', `${i}`);
});

$(navList).click(showText);

/*открытие и подсветка первого списка меню при загрузке страницы*/
navItems[0].click();

function showText(event) {
    /*подсветка выбранного пункта меню */
    if (prevMenuItem) {
        $(prevMenuItem).removeClass('active');
    }

    $.each(navItems, (i, item) => {
        if ($(item).hasClass('active')) {
            $(item).removeClass('active');
        }
    });

    let activeMenuItem = event.target;
    $(activeMenuItem).addClass('active');
    prevMenuItem = activeMenuItem;

    /*показ текста*/

    let tabNumber = $(activeMenuItem).attr('dataNumber');

    if (prevText) {
        $(prevText).hide();
    }
    $.each(contentListItems, (i, item) => {
        if ($(item).attr('dataNumber') === tabNumber) {
            $(item).show();
            prevText = item;
        }
    });
}



